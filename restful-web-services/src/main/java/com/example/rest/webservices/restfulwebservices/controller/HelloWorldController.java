package com.example.rest.webservices.restfulwebservices.controller;

import org.springframework.web.bind.annotation.*;

//Controller
@RestController
public class HelloWorldController {

    //Get
    //URI - /hello-world
    //method - Hello World
    @GetMapping(value = "/hello-world")
    public String helloWorld(){
        return "Hello World";
    }

    @GetMapping(value = "/hello-world-bean")
    public HelloWorldBean helloWorldBean(){
        return new HelloWorldBean("Hello World");
    }

    @GetMapping(value = "/hello-world-bean/path-variable/{name}")
    public HelloWorldBean helloWorldPathVariable(@PathVariable String name ){
        return new HelloWorldBean(String.format("Hello World, %s", name));
    }

}
