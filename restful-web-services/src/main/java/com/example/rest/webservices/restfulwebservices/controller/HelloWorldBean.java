package com.example.rest.webservices.restfulwebservices.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class HelloWorldBean {

    private  String message;

    @Override
    public String toString(){
        return  String.format("HelloWorldBean [message=%s]" , message);
    }
}
